package com.hasib.guskitchen.data.source.remote

import com.hasib.guskitchen.model.Character
import retrofit2.http.GET
import retrofit2.http.Path

interface CharacterApiService {

    @GET("characters")
    suspend fun allCharacters(): List<Character>

    @GET("characters/{id}")
    suspend fun particularCharacters(@Path("id") id: Int): Character

    @GET("characters/random")
    suspend fun randomCharacters(): Character
}