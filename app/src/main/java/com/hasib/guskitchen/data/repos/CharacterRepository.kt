package com.hasib.guskitchen.data.repos

import com.hasib.guskitchen.helpers.Results
import com.hasib.guskitchen.model.Character

interface CharacterRepository {

    suspend fun loadAllCharacters(): Results<List<Character>>
}