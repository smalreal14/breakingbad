package com.hasib.guskitchen.ui.characters.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.hasib.guskitchen.databinding.ItemCharacterBinding
import com.hasib.guskitchen.model.Character
import timber.log.Timber

class CharacterAdapter(
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mDiffer = AsyncListDiffer(this, CharacterDiffUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemCharacterBinding = ItemCharacterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CharacterViewHolder(itemCharacterBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val item = mDiffer.currentList[position]
            (holder as CharacterViewHolder).bind(item)
    }

    override fun getItemCount(): Int {
        Timber.d(mDiffer.currentList.size.toString())
        return mDiffer.currentList.size
    }

    fun submitList(list: List<Character>) {
        return mDiffer.submitList(list)
    }

    inner class CharacterViewHolder(
        private val itemCharacterBinding: ItemCharacterBinding
    ) : RecyclerView.ViewHolder(itemCharacterBinding.root) {
        fun bind(character: Character) {
            itemCharacterBinding.character = character
        }
    }
}