package com.hasib.guskitchen.ui.characters

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hasib.guskitchen.data.repos.CharacterRepository
import com.hasib.guskitchen.helpers.Results
import com.hasib.guskitchen.model.Character
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CharacterViewModel @Inject constructor(
    private val repository: CharacterRepository
) : ViewModel() {

    private val _loading = MutableLiveData(true)
    val loading: LiveData<Boolean> = _loading

    private val _characters = MutableLiveData<List<Character>>()
    val characters: LiveData<List<Character>> = _characters

    private val _errorResult = MutableLiveData<String>()
    val errorResult: LiveData<String> = _errorResult

    fun loadCharacters() {
        viewModelScope.launch {
            _loading.postValue(true)

            when(val result = repository.loadAllCharacters()) {
                is Results.Success -> {
                    _loading.postValue(false)
                    _characters.postValue(result.value)
                }

                is Results.Failure -> {
                    _loading.postValue(false)
                    _errorResult.postValue(result.throwable.toString())
                    Timber.w(result.throwable)
                }
            }
        }
    }
}