package com.hasib.guskitchen.helpers

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.facebook.shimmer.ShimmerFrameLayout

@BindingAdapter("shimmering")
fun ShimmerFrameLayout.controlShimmering(isFetching: Boolean) {
    if (isFetching) {
        startShimmer()
    } else {
        stopShimmer()
        hideShimmer()
        visibility = View.GONE
    }
}

@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(url: String) {
    Glide
        .with(context)
        .load(url)
        .placeholder(ColorDrawable(Color.GRAY))
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .circleCrop()
        .into(this)
}