package com.hasib.guskitchen.helpers

import timber.log.Timber

object ResolveApiResponse {

    suspend fun <T> resolve(updateCall: suspend () -> T) =
        try {
            Results.Success(updateCall.invoke())
        } catch (throwable: Throwable) {
            Timber.w(throwable)
            throwable.printStackTrace()
            Results.Failure(throwable)
        }
}