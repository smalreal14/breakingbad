package com.hasib.guskitchen.di

import com.hasib.guskitchen.data.repos.CharacterRepository
import com.hasib.guskitchen.data.repos.CharacterRepositoryImpl
import com.hasib.guskitchen.data.source.remote.CharacterApiService
import com.hasib.guskitchen.helpers.ResolveApiResponse
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Singleton
    @Provides
    fun provideCharacterRepository(
        characterApiService: CharacterApiService,
        resolveApiResponse: ResolveApiResponse
    ): CharacterRepository = CharacterRepositoryImpl(characterApiService, resolveApiResponse)

    @Provides
    fun provideResolveApiResponse(): ResolveApiResponse = ResolveApiResponse

    @Singleton
    @Provides
    fun provideCharacterApiService(retrofit: Retrofit): CharacterApiService =
        retrofit.create(CharacterApiService::class.java)

}